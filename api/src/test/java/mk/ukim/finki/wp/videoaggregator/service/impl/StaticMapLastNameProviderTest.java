package mk.ukim.finki.wp.videoaggregator.service.impl;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StaticMapLastNameProviderTest {

    private StaticMapLastNameProvider lastNameProvider;

    @Before
    public void setup() {

        lastNameProvider = new StaticMapLastNameProvider();
    }

    @Test
    public void test_last_name() {
        String actual = lastNameProvider.lastName("Riste");
        assertEquals("Last names should be equal", "Stojanov", actual);
    }

    @Test
    public void test_non_existing_last_name() {
        String actual = lastNameProvider.lastName("non existing");
        assertNull(actual);
    }

}