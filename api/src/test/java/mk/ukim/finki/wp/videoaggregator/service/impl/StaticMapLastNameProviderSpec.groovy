package mk.ukim.finki.wp.videoaggregator.service.impl

import spock.lang.Specification
import spock.lang.Unroll

class StaticMapLastNameProviderSpec extends Specification {
    StaticMapLastNameProvider lastNameProvider

    def setup() {
        lastNameProvider = new StaticMapLastNameProvider()
    }

    @Unroll
    def "last name for '#firstName' should be '#lastName'"() {
        expect:
        def actual = lastNameProvider.lastName(firstName)
        actual == lastName

        where:
        firstName | lastName
        "Riste"   | "Stojanov"
        "Dimitar" | "Trajanov"
    }
}
