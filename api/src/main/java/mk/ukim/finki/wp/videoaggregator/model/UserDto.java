package mk.ukim.finki.wp.videoaggregator.model;

import lombok.Getter;
import lombok.Setter;
import mk.ukim.finki.wp.videoaggregator.web.validators.PasswordMatches;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@PasswordMatches
@Getter @Setter
public class UserDto {

  @NotNull
  @NotEmpty
  public String username;

  @NotNull
  @NotEmpty
  public String password;

  public String matchingPassword;

  @NotNull
  @NotEmpty
  public String email;

}
