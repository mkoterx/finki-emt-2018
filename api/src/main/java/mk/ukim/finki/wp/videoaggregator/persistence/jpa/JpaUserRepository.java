package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.persistence.UserRepository;
import org.springframework.data.repository.Repository;

public interface JpaUserRepository extends UserRepository, Repository<User, Long> {
}
