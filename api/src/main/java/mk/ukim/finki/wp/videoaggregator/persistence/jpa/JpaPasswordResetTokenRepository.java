package mk.ukim.finki.wp.videoaggregator.persistence.jpa;

import mk.ukim.finki.wp.videoaggregator.model.PasswordResetToken;
import mk.ukim.finki.wp.videoaggregator.persistence.PasswordResetTokenRepository;
import org.springframework.data.repository.Repository;

public interface JpaPasswordResetTokenRepository extends PasswordResetTokenRepository, Repository<PasswordResetToken, Long> {
}
