package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.*;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.DuplicateEmailException;
import mk.ukim.finki.wp.videoaggregator.persistence.PasswordResetTokenRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.VerificationTokenRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.jpa.JpaUserRepository;
import mk.ukim.finki.wp.videoaggregator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

  public static final String INVALID_TOKEN = "invalidToken";
  public static final String EXPIRED_TOKEN = "expired";
  public static final String VALID_TOKEN = "valid";

  private final JpaUserRepository userRepository;

  private final PasswordEncoder passwordEncoder;
  private final VerificationTokenRepository verificationTokenRepository;
  private final PasswordResetTokenRepository passwordResetTokenRepository;

  @Autowired
  public UserServiceImpl(JpaUserRepository repository, PasswordEncoder passwordEncoder, VerificationTokenRepository verificationTokenRepository, PasswordResetTokenRepository passwordResetTokenRepository) {
    this.userRepository = repository;
    this.passwordEncoder = passwordEncoder;
    this.verificationTokenRepository = verificationTokenRepository;
    this.passwordResetTokenRepository = passwordResetTokenRepository;
  }

  @Override
  public User registerNewUser(UserDto user) throws DuplicateEmailException {
    if(this.checkDuplicateEmail(user.getEmail())){
      throw new DuplicateEmailException();
    }
    User u = new User();
    String encodedPass = passwordEncoder.encode(user.getPassword());
    u.setUsername(user.getUsername());
    u.setEmail(user.getEmail());
    u.setPassword(encodedPass);
    u.setRole(Role.ROLE_WEB_USER);
    return userRepository.save(u);
  }

  @Override
  public User saveUser(UserDto user){
    User u = new User();
    u.setUsername(user.getUsername());
    u.setEmail(user.getEmail());
    u.setPassword(passwordEncoder.encode(user.getPassword()));
    u.setRole(Role.ROLE_WEB_USER);
    return userRepository.save(u);
  }

  @Override
  public User save(User user) {
    return userRepository.save(user);
  }

  @Override
  public User getUser(String token) {
    UserVerificationToken foundToken = verificationTokenRepository.findByToken(token);
    if(foundToken != null){
      return foundToken.getUser();
    }
    return null;
  }

  @Override
  public UserVerificationToken getToken(String token) {
    return verificationTokenRepository.findByToken(token);
  }

  @Override
  public void createVerificationToken(User user, String randomToken) {
    UserVerificationToken token = new UserVerificationToken();
    token.setToken(randomToken);
    token.setUser(user);
    verificationTokenRepository.save(token);
  }

  @Override
  public UserVerificationToken generateNewVerificationToken(String verificationToken) {
    UserVerificationToken vToken = verificationTokenRepository.findByToken(verificationToken);
    vToken.setToken(UUID.randomUUID().toString());
    vToken = verificationTokenRepository.save(vToken);
    return vToken;
  }

  @Override
  public String validateVerificationToken(String token) {

    UserVerificationToken userToken = verificationTokenRepository.findByToken(token);
    if(userToken == null){
      return INVALID_TOKEN;
    }

    User user = userToken.getUser();
    Calendar calendar = Calendar.getInstance();
    if(userToken.getExpiryDate().getTime() - calendar.getTime().getTime() <= 0){
      verificationTokenRepository.delete(userToken);
      return EXPIRED_TOKEN;
    }

    user.setActive(true);
    userRepository.save(user);
    return VALID_TOKEN;

  }

  @Override
  public User findByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  @Override
  public Optional<User> findByEmail(String email){
    return userRepository.findByEmail(email);
  }

  @Override
  public User findById(Long id) {
    return userRepository.findById(id);
  }

  @Override
  public void createPasswordResetTokenForUser(User user, String token) {
    PasswordResetToken passwordResetToken = new PasswordResetToken();
    passwordResetToken.setToken(token);
    passwordResetToken.setUser(user);
    passwordResetTokenRepository.save(passwordResetToken);
  }

  @Override
  public String validatePasswordResetToken(Long id, String token) {
    PasswordResetToken passToken = passwordResetTokenRepository.findByToken(token);
    if ((passToken == null) || (!Objects.equals(passToken.getUser().getId(), id))) {
      return INVALID_TOKEN;
    }

    Calendar cal = Calendar.getInstance();
//    if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
//      return EXPIRED_TOKEN;
//    }

    User user = passToken.getUser();
    Authentication auth = new UsernamePasswordAuthenticationToken(user, null, Arrays.asList(new SimpleGrantedAuthority("CHANGE_PASSWORD_AUTHORITY")));
    SecurityContextHolder.getContext().setAuthentication(auth);
    return null;
  }

  @Override
  public void changeUserPassword(User user, String password) {
    user.setPassword(passwordEncoder.encode(password));
    userRepository.save(user);
  }

  @Override
  public long numberOfUsers() {
    return userRepository.count();
  }


  private boolean checkDuplicateEmail(final String email) {
    return userRepository.findByEmail(email).isPresent();
  }

}
