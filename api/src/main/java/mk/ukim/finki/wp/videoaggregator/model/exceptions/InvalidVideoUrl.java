package mk.ukim.finki.wp.videoaggregator.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Riste Stojanov
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidVideoUrl extends RuntimeException {
}
