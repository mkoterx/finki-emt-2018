package mk.ukim.finki.wp.videoaggregator.persistence.testing;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.Random;

@Deprecated
public class MockVideoRepository implements VideoRepository {
    private static final Logger logger = LoggerFactory.getLogger(MockVideoRepository.class);

    @Override
    public Video save(Video video) {
        logger.debug("Saving video [{}]", video);
        video.id = new Random().nextLong();
        return video;
    }

    @Override
    public Optional<Video> findOne(Long id) {
        return Optional.empty();
    }

  @Override
  public Optional<Video> findOneVideoWithTags(Long id) {
    return Optional.empty();
  }

    @Override
    public void delete(Video video) {

    }

  @Override
  public Iterable<Video> findByCategoryTitle(String title) {
    return null;
  }

  @Override
  public Iterable<Video> findAll() {
    return null;
  }

  @Override
  public Page<Video> findAll(Pageable pageable) {
    return null;
  }

  @Override
  public Iterable<Video> findAllFetchingAllProperties() {
    return null;
  }

  @Override
  public Iterable<Video> findByTagsName(String tagName) {
    return null;
  }

  @Override
  public Iterable<Video> findByTitleIgnoreCaseContaining(String title) {
    return null;
  }

  @Override
  public Page<Video> findAll(Example<Video> example, Pageable pageable) {
    return null;
  }
}
